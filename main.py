#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 20 14:09:31 2019

@author: Adelino (based on Krishna work)
"""
# -------------------------------
import matplotlib
import matplotlib.pyplot as plt
# -------------------------------
import os
import librosa
import numpy as np
from utils import utils
from utils.utils import speech_collate
# import pickle
import sys
# -------------------------------
from utils.filemisc import readFilesFromPath
from SpeechDataGenerator import SpeechDataGenerator
from utils.featuremisc import feature_extraction, compute_and_save_raw_features, build_and_save_UBM, normalize_and_save_features
# -------------------------------
import torch
from torch import optim
import torch.nn as nn
from torch.utils.data import DataLoader
# -----------------------------------------------------------------------------
from sklearn.metrics import accuracy_score
# -----------------------------------------------------------------------------
from models.x_vector import X_vector

torch.multiprocessing.set_sharing_strategy('file_system')
torch.set_num_threads(16)

computeQSTFeatures = False
tamanhoLote = 39
last_loss   = 1e6
last_acc    = 0;


# -----------------------------------------------------------------------------
def train(dataloader_train,epoch):
    train_loss_list=[]
    full_preds=[]
    full_gts=[]
    model.train()
    for i_batch, sample_batched in enumerate(dataloader_train):
    
        features = torch.from_numpy(np.asarray([torch_tensor.numpy().T for torch_tensor in sample_batched[0]])).float()
        labels = torch.from_numpy(np.asarray([torch_tensor[0].numpy() for torch_tensor in sample_batched[1]]))
        features, labels = features.to(device),labels.to(device)
        features.requires_grad = True
        optimizer.zero_grad()
        pred_logits,x_vec = model(features)
        #### CE loss
        loss = loss_fun(pred_logits,labels)
        loss.backward()
        optimizer.step()
        train_loss_list.append(loss.item())
        #train_acc_list.append(accuracy)
        #if i_batch%10==0:
        #    print('Loss {} after {} iteration'.format(np.mean(np.asarray(train_loss_list)),i_batch))
        
        predictions = np.argmax(pred_logits.detach().cpu().numpy(),axis=1)
        for pred in predictions:
            full_preds.append(pred)
        for lab in labels.detach().cpu().numpy():
            full_gts.append(lab)
            
    mean_acc = accuracy_score(full_gts,full_preds)
    mean_loss = np.mean(np.asarray(train_loss_list))
    print('Total training   loss {0:8.6f} and training   Accuracy {1:5.3f} after {2:04d} epochs'.format(mean_loss,mean_acc,epoch))
    

# -----------------------------------------------------------------------------
def validation(dataloader_val,epoch, last_loss, last_acc):
    model.eval()
    with torch.no_grad():
        val_loss_list=[]
        full_preds=[]
        full_gts=[]
        for i_batch, sample_batched in enumerate(dataloader_val):
            features = torch.from_numpy(np.asarray([torch_tensor.numpy().T for torch_tensor in sample_batched[0]])).float()
            labels = torch.from_numpy(np.asarray([torch_tensor[0].numpy() for torch_tensor in sample_batched[1]]))
            features, labels = features.to(device),labels.to(device)
            pred_logits,x_vec = model(features)
            #### CE loss
            loss = loss_fun(pred_logits,labels)
            val_loss_list.append(loss.item())
            #train_acc_list.append(accuracy)
            predictions = np.argmax(pred_logits.detach().cpu().numpy(),axis=1)
            for pred in predictions:
                full_preds.append(pred)
            for lab in labels.detach().cpu().numpy():
                full_gts.append(lab)
                
        mean_acc = accuracy_score(full_gts,full_preds)
        mean_loss = np.mean(np.asarray(val_loss_list))
        print('Total validation loss {0:8.6f} and Validation accuracy {1:5.3f} after {2:04d} epochs'.format(mean_loss,mean_acc,epoch))
        
        if ((mean_acc > last_acc) | (mean_loss < last_loss)):
        # if ((mean_acc > last_acc) | ((mean_loss < last_loss) & (mean_acc > 0.975*last_acc))):
            model_save_path = os.path.join('save_model', 'best_check_point_{0:8.6f}_{1:5.3f}_{2:04d}'.format(mean_loss,mean_acc,epoch))
            state_dict = {'model': model.state_dict(),'optimizer': optimizer.state_dict(),'epoch': epoch}
            torch.save(state_dict, model_save_path)
            return mean_loss, mean_acc
        
        return last_loss, last_acc
        
        
# -----------------------------------------------------------------------------
if (__name__ == '__main__'):
    # dirGSM = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/GSM"
    # dirPDR = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/PDR"
    # dirQST = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/QST"
    dirGSM = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/GSM_debug"
    dirPDR = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/PDR_debug"
    dirQST = "/media/adelino/SMAUG/Verificacao_Locutor_SPAV/UBM/QST_debug"
    CURR_DIR = os.getcwd()
    dataFolder = CURR_DIR + "/data"
    gsmFileList = readFilesFromPath(dirGSM)
    pdrFileList = readFilesFromPath(dirPDR)
    qstFileList = readFilesFromPath(dirQST)
    RAW_PDR_MFCC_FILE   = "/RAW_PDR_MFCC.txt"
    RAW_PDR_VAD_FILE    = "/RAW_PDR_VAD.txt"
    RAW_GSM_MFCC_FILE   = "/RAW_GSM_MFCC.txt"
    RAW_GSM_VAD_FILE    = "/RAW_GSM_VAD.txt"
    RAW_QST_MFCC_FILE   = "/RAW_QST_MFCC.txt"
    RAW_QST_VAD_FILE    = "/RAW_QST_VAD.txt"

    UBM_PDR_FILE = "/UBM_PDR_MFCC.txt"
    UBM_GSM_FILE = "/UBM_GSM_MFCC.txt"
    UBM_QST_FILE = "/UBM_QST_MFCC.txt"
    NORM_PDR_MFCC_FILE   = "/NORM_PDR_MFCC.txt"
    NORM_GSM_MFCC_FILE   = "/NORM_GSM_MFCC.txt"
    NORM_QST_MFCC_FILE   = "/NORM_QST_MFCC.txt"

    if not os.path.exists(dataFolder):
        os.makedirs(dataFolder)

    compute_and_save_raw_features(dataFolder,pdrFileList,RAW_PDR_MFCC_FILE,RAW_PDR_VAD_FILE)
    compute_and_save_raw_features(dataFolder,gsmFileList,RAW_GSM_MFCC_FILE,RAW_GSM_VAD_FILE)
    UBM_PDR = build_and_save_UBM(dataFolder,RAW_PDR_MFCC_FILE, RAW_PDR_VAD_FILE,UBM_PDR_FILE)
    UBM_GSM = build_and_save_UBM(dataFolder,RAW_GSM_MFCC_FILE, RAW_GSM_VAD_FILE,UBM_GSM_FILE)
    normalize_and_save_features(dataFolder,RAW_PDR_MFCC_FILE,RAW_PDR_VAD_FILE,UBM_PDR_FILE,NORM_PDR_MFCC_FILE)
    normalize_and_save_features(dataFolder,RAW_GSM_MFCC_FILE,RAW_GSM_VAD_FILE,UBM_GSM_FILE,NORM_GSM_MFCC_FILE)
    if (computeQSTFeatures):
        compute_and_save_raw_features(dataFolder,qstFileList,RAW_QST_MFCC_FILE,RAW_QST_VAD_FILE)

    
## Model related
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    model = X_vector(tamanhoLote, len(gsmFileList)).to(device)
    optimizer = optim.Adam(model.parameters(), lr=0.001, weight_decay=0.0, betas=(0.9, 0.98), eps=1e-6)
    loss_fun = nn.CrossEntropyLoss()

    dataset_train = SpeechDataGenerator(listOfFiles=gsmFileList,mode='train',filedata=dataFolder+NORM_GSM_MFCC_FILE)
    dataloader_train = DataLoader(dataset_train, batch_size=tamanhoLote,shuffle=True,collate_fn=speech_collate) 
    
    dataset_val = SpeechDataGenerator(listOfFiles=pdrFileList,mode='train',filedata=dataFolder+NORM_PDR_MFCC_FILE)
    dataloader_val = DataLoader(dataset_train, batch_size=tamanhoLote,shuffle=True,collate_fn=speech_collate) 
    print("Antes do treino")
    
    for epoch in range(30):
        train(dataloader_train,epoch)
        last_loss,last_acc = validation(dataloader_val,epoch,last_loss,last_acc)
