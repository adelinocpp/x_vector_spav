# x_vector_SPAV
Implementation of x-vector for forensic speaker identification

Based on work of: 

Manoj Kumar: https://github.com/manojpamk/pytorch_xvectors

Krishna: https://github.com/KrishnaDN/x-vector-pytorch
