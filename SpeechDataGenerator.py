import pickle
import numpy as np
import torch
from utils import utils
from utils import featuremisc
# -----------------------------------------------------------------------------
class SpeechDataGenerator():
    def __init__(self, listOfFiles, mode, filedata):
        """
        Read the textfile and get the paths
        """
        self.mode=mode
        self.audio_links = listOfFiles
        self.labels = [int(i) for i in range(0,len(listOfFiles))]
        self.filedata = filedata
    
    def __len__(self):
        return len(self.audio_links)

    def __getitem__(self, idx):
        audio_link =self.audio_links[idx]
        class_id = self.labels[idx]
        spec = featuremisc.load_norm_mfcc(class_id,fileData=self.filedata, mode=self.mode)        
        sample = {'features': torch.from_numpy(np.ascontiguousarray(spec)), 'labels': torch.from_numpy(np.ascontiguousarray(class_id))}        
        return sample